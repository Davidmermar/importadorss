create table autor(
idautor  varchar(5) primary key,
nickname varchar(15),
email nvarchar(50),
)

create table categorias(
idcategoria varchar(5) primary key,
nombre varchar(20),
)

create table post(
idpost varchar(5) primary key,
fecha datetime,
titulo  nvarchar (50),
descripcion nvarchar (4000),
idcategoria varchar (5),
CONSTRAINT fk_categoria1 FOREIGN KEY (idcategoria) REFERENCES categorias (idcategoria),
idautor varchar (5),
CONSTRAINT fk_autor2 FOREIGN KEY (idautor) REFERENCES autor (idautor),

)